##History##

0.1 Version

Installation
============

Step 1: Download the Bundle
---------------------------

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```bash
$ composer require alpeinsoft/apiloan "~1"
```

This command requires you to have Composer installed globally, as explained
in the [installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

Step 2: Enable the Bundle
-------------------------

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Alpeinsoft\LoanBundle\AlpeinsoftLoanBundle(),
        );
    }
}
```

Step 3: Enable bundle routing
-------------------------
```yml
<?php
// app/config/routing.yml

alpeinsoft_apiloan:
    resource: "@AlpeinsoftLoanBundle/Resources/config/routing.yml"
    prefix:   /

```

Step 4: Configure
-------------------------
```yml
<?php
// app/config/config.yml

alpeinsoft_loan:
    loan_entity_class: class
    loan_repository_class: class
    customer_entity_class: class
    backend_layout_path: path

```