<?php

namespace  Alpeinsoft\LoanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Fisan\Entity\Traits\Timestampable;

/**
 * LoanRule.
 *
 * @ORM\Table(name="loan_rules")
 * @ORM\Entity(repositoryClass="Alpeinsoft\LoanBundle\Entity\Repository\LoanRuleRepository")
 */
class LoanRule
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="service_fee", type="integer")
     */
    private $serviceFee;

    /**
     * @var float
     *
     * @ORM\Column(name="total_sum", type="float")
     */
    private $totalSum;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer")
     */
    private $duration;

    /**
     * @var int
     *
     * @ORM\Column(name="debd_amount", type="integer")
     */
    private $debdAmount;

    /**
     * @var int
     *
     * @ORM\Column(name="fee", type="integer")
     */
    private $fee;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serviceFee.
     *
     * @param int $serviceFee
     *
     * @return LoanRule
     */
    public function setServiceFee($serviceFee)
    {
        $this->serviceFee = $serviceFee;

        return $this;
    }

    /**
     * Get serviceFee.
     *
     * @return int
     */
    public function getServiceFee()
    {
        return $this->serviceFee;
    }

    /**
     * Set totalSum.
     *
     * @param float $totalSum
     *
     * @return LoanRule
     */
    public function setTotalSum($totalSum)
    {
        $this->totalSum = $totalSum;

        return $this;
    }

    /**
     * Get totalSum.
     *
     * @return float
     */
    public function getTotalSum()
    {
        return $this->totalSum;
    }

    /**
     * Set duration.
     *
     * @param int $duration
     *
     * @return LoanRule
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration.
     *
     * @return int
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set debdAmount.
     *
     * @param int $debdAmount
     *
     * @return LoanRule
     */
    public function setDebdAmount($debdAmount)
    {
        $this->debdAmount = $debdAmount;

        return $this;
    }

    /**
     * Get debdAmount.
     *
     * @return int
     */
    public function getDebdAmount()
    {
        return $this->debdAmount;
    }

    /**
     * Set fee.
     *
     * @param int $fee
     *
     * @return LoanRule
     */
    public function setFee($fee)
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * Get fee.
     *
     * @return int
     */
    public function getFee()
    {
        return $this->fee;
    }
}
