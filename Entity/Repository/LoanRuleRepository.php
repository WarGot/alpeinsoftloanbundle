<?php

namespace  Alpeinsoft\LoanBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class LoanRuleRepository extends EntityRepository
{
    public function queryBy($criteria = [])
    {
        $queryBuilder = $this->createQueryBuilder('lr');

        if (isset($criteria['query'])) {
            $queryBuilder
                ->where('lr.debdAmount LIKE :query')
                ->setParameter('query', '%'.$criteria['query'].'%')
            ;
        }

        return $queryBuilder->getQuery();
    }

    public function queryAll()
    {
        return $this->createQueryBuilder('lr')
            ->getQuery()
        ;
    }

    public function searchByDebt($debt)
    {
        return $this->createQueryBuilder('lr')
            ->where('lr.debdAmount >= :debt')
            ->setParameter('debt', $debt ? $debt : 0)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function allDebtArray()
    {
        return $this->createQueryBuilder('lr')
            ->select('lr.debdAmount')
            ->addSelect('lr.fee')
            ->orderBy('lr.debdAmount')
            ->getQuery()
            ->getArrayResult()
        ;
    }

    protected function applyCriteria(QueryBuilder $queryBuilder, array $criteria = null)
    {
        if (null === $criteria) {
            return;
        }

        foreach ($criteria as $property => $value) {
            if (!$this->isProperty($property)) {
                continue;
            }

            if (null === $value) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->isNull($this->getPropertyName($property)));
            } elseif (is_array($value)) {
                $queryBuilder->andWhere($queryBuilder->expr()->in($this->getPropertyName($property), $value));
            } elseif ('' !== $value) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->eq($this->getPropertyName($property), ':'.$property))
                    ->setParameter($property, $value);
            }
        }
    }

    public function isProperty($property)
    {
        return property_exists($this->getEntityName(), $property);
    }

    protected function getPropertyName($name)
    {
        if (false === strpos($name, '.')) {
            return $this->getAlias().'.'.$name;
        }

        return $name;
    }

    /**
     * {@inheritdoc}
     */
    public function createNew()
    {
        $className = $this->getClassName();

        return new $className();
    }

    /**
     * {@inheritdoc}
     */
    public function save($object, $flush = true)
    {
        $this->_em->persist($object);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function remove($object, $flush = true)
    {
        $this->_em->remove($object);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function getAlias()
    {
        return 'o';
    }
}
