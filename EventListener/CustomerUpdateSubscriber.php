<?php

namespace Alpeinsoft\LoanBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class CustomerUpdateSubscriber
{
    protected $loanEntity;
    protected $customerEntity;
    protected $container;
    public function __construct(Container $container, $loanEntity, $customerEntity){
        $this->container = $container;
        $this->loanEntity = $loanEntity;
        $this->customerEntity = $customerEntity;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $this->recalculateLoan($args);
    }
    public function postUpdate(LifecycleEventArgs $args){
        $this->recalculateLoan($args);
    }

    protected function recalculateLoan(LifecycleEventArgs $args){
        $entity = $args->getEntity();
        // Only for Customer entity
        if (!$entity instanceof $this->customerEntity) {
            return;
        }
        $loanService = $this->container->get('alpeinsoft.service.loan');
        $em = $args->getEntityManager();
        $customer = $args->getEntity();
        $result = $loanService->getPayments($customer->getClientDebtAmount(), $customer->getMonthlyInstallment());
        $customer->setTotalSum($result['total']);
        $customer->setMonthlyInstallment($result['month']);
        $customer->setDuration($result['duration']);
        $customer->setClientDebtAmount($result['debtAmount']);
        $em->persist($customer);
        $em->flush();
    }
}
