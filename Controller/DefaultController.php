<?php

namespace Alpeinsoft\LoanBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/path")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="name_list")
     * @Template()
     */
    public function indexAction()
    {
        return [

        ];
    }

    /**
     * @Route("/show/{id}", name="name_show")
     * @Template()
     */
    public function showAction($id)
    {
        return [

        ];
    }

    /**
     * @Route("/add", name="name_add")
     * @Template()
     */
    public function addAction()
    {
        return [

        ];
    }

    /**
     * @Route("/create", name="name_create")
     * @Template()
     */
    public function createAction()
    {
        return [

        ];
    }

    /**
     * @Route("/edit/{id}", name="name_edit")
     * @Template()
     */
    public function editAction($id)
    {
        return [

        ];
    }

    /**
     * @Route("/update/{id}", name="name_update")
     * @Template()
     */
    public function updateAction($id)
    {
        return [

        ];
    }

    /**
     * @Route("/delete/{id}", name="name_delete")
     * @Template()
     */
    public function deleteAction($id)
    {
        return [

        ];
    }
}
