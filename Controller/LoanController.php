<?php

namespace Alpeinsoft\LoanBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LoanController extends Controller
{
    public function allAction(Request $request)
    {
        $loanService = $this->get('alpeinsoft.service.loan');
        $loanRules = $loanService->getLoans($request);

        return $this->render('AlpeinsoftLoanBundle::all.html.twig', [
            'laonRules' => $this->get('knp_paginator')->paginate(
                $loanRules,
                $request->get('page', 1),
                $request->get('limit', 20)
            ),
            'parentTemplate' => $this->getParameter('alpeinsoft.api.backend_layout_path'),
        ]);
    }
}
