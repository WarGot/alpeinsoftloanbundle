<?php

namespace Alpeinsoft\LoanBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('alpeinsoft_loan');

        $rootNode->children()
            ->scalarNode('backend_layout_path')
                ->isRequired()
                ->cannotBeEmpty()
            ->end()
            ->scalarNode('loan_entity_class')
                ->defaultValue('Alpeinsoft\LoanBundle\Entity\LoanRule')
                //->isRequired()
            ->end()
            ->scalarNode('loan_repository_class')
                ->defaultValue('Alpeinsoft\LoanBundle\Entity\Repository\LoanRuleRepository')
                //->isRequired()
            ->end()
            ->scalarNode('customer_entity_class')
                ->defaultValue('Fisan\Entity\Customer')
            ->end()
        ->end();

        return $treeBuilder;
    }
}
