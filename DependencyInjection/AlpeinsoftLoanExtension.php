<?php

namespace Alpeinsoft\LoanBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class AlpeinsoftLoanExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        $container->setParameter('alpeinsoft.api.loan_entity_class', $config['loan_entity_class']);
        $container->setParameter('alpeinsoft.api.loan_repository_class', $config['loan_repository_class']);
        $container->setParameter('alpeinsoft.api.customer_entity_class', $config['customer_entity_class']);
        $container->setParameter('alpeinsoft.api.backend_layout_path', $config['backend_layout_path']);
    }
}
