<?php

namespace Alpeinsoft\LoanBundle\Service;

use Symfony\Component\HttpFoundation\Request;

class LoanService
{
    protected $em;
    protected $loanEntity;
    protected $loanRepository;

    public function __construct($doctrine, $entityClass, $repositoryClass, $parentTemplate)
    {
        $this->em = $doctrine->getEntityManager();
        $this->loanEntity = $entityClass;
        $this->loanRepository = $repositoryClass;
    }

    /**
     * {@inheritdoc}
     */
    public function getLoans(Request $request)
    {
        $loanRules = $this->em->getRepository($this->loanEntity)->queryBy($request->get('criteria', []));

        return $loanRules;
    }

    /**
     * [getPayments description].
     *
     * @param [type] $customer [description]
     *
     * @return [type] [description]
     *                duration = total_sum / rate (округлить до целого)
     *                monthly_installment = totalSum / duraiton
     *                monthly_installment округлить до 5
     */
    public function getPayments($debtAmount, $rate)
    {
        $loan = $this->em->getRepository($this->loanEntity)->searchByDebt($debtAmount);

        $totalSum = $loan->getTotalSum();
        $duration = round($totalSum / $rate);
        $monthlyInstallment = $this->roundUpTo5Cents($totalSum / $duration);
        $totalSum = round($duration * $monthlyInstallment, 1);

        return ['total' => $totalSum, 'month' => $monthlyInstallment, 'duration' => $duration, 'debtAmount' => $debtAmount];
    }

    protected function roundUpTo5Cents($value)
    {
        $valueInString = strval(round($value, 2));
        if (strpos($valueInString, '.') == 0) {
            $valueInString = $valueInString.'.00';
        }
        $valueArray = explode('.', $valueInString);
        $substringValue = substr($valueArray[1], 1);

        if ($substringValue >= 1 && $substringValue <= 5) {
            $tempValue = str_replace(substr($valueArray[1], 1), 5, substr($valueArray[1], 1));
            $tempValue = substr($valueArray[1], 0, 1).$tempValue;
            $newvalue = floatval($valueArray[0].'.'.$tempValue);
        } elseif (0 == $substringValue) {
            $newvalue = floatval($value);
        } else {
            $newFloat = floatval($valueArray[0].'.'.substr($valueArray[1], 0, 1));
            $newvalue = ($newFloat + 0.1);
        }

        return $newvalue;
    }

    public function allDebtArray()
    {
        $result = $this->em->getRepository($this->loanEntity)->allDebtArray();
        return $result;
    }
}
